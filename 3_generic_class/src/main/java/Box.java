import java.util.ArrayList;
import java.util.List;

/**
 * Generieke multiklasse
 */
public class Box {
    private List<String> myList = new ArrayList<>();

    public void add(String str) {
        myList.add(str);
    }

    public String get(int i) {
        return myList.get(i);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (String str : myList) {
            sb.append(str + " ");
        }
        return sb.toString();
    }
}