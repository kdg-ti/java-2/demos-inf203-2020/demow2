import java.util.ArrayList;
import java.util.List;

/**
 * Mark Goovaerts
 * 23/09/2019.
 */
public class DemoGenerics {
    public static void main(String[] args) {
        List myList = new ArrayList();
        myList.add("test");
        myList.add(4);

        System.out.println("OK");
    }
}
